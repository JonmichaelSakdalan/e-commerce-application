import React,{useContext,useEffect} from 'react';
import {Alert} from 'react-bootstrap';
import UserContext from '../userContext'
import {Redirect,useHistory} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Logout(){

	const {setUser,unsetUser} = useContext(UserContext);

	const history = useHistory();

	unsetUser();
	useEffect(()=>{
		setUser({
			id:null,
			isAdmin:null

		})	

	},[])
	
	Swal.fire({
		icon: "success",
		title: "Thank you my senpai."
	})
	history.push("/") 
	return(
		<Alert>
		</Alert>
	)
}
