import {Form, Button, Container} from 'react-bootstrap';
import React,{useState,useEffect,useContext} from 'react';
import {Redirect,useHistory} from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import '../srcCSS/universal.css';

export default function Login(){

	const {user,setUser} = useContext(UserContext);

	const history = useHistory();

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{

		if(email !== "" && password !==""){

			setIsActive(true)

		} else {

			setIsActive(false)
			
		}

		},[email,password])

	function loginUser(e){

		e.preventDefault()

		fetch('https://sheltered-peak-35807.herokuapp.com/users/login',{
			method: 'POST',
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})

		})
		.then(res=>res.json())
		.then(data=>{
			if(data.accessToken){
				Swal.fire({
					icon: "success",
					title: "Login Successful!",
					text: `You are now logged in`
				})
				setEmail("")
				setPassword("")
				history.push('/viewAll')

				localStorage.setItem('token',data.accessToken);
				
				fetch('https://sheltered-peak-35807.herokuapp.com/users/getUserDetails',{

					headers: {

						'Authorization': `Bearer ${data.accessToken}`

					}

				})
				.then(res => res.json())
				.then(data => {

					setUser({

						id: data._id,
						isAdmin: data.isAdmin

					})

				})

			}
			else{

				Swal.fire({
					icon: "error",
					title: "Login Failed",
					text: data.message
				})

			}
		})
	}

	return(
		<Container style={{ backgroundImage:"url('https://wallpapercave.com/wp/wp2745298.jpg')"}}>
		<div className='center'>
		<img src="https://randmsgbucket.s3.us-west-1.amazonaws.com/2cb59578-dd6a-49a7-9894-49e290c0a1cd.jpg" alt="alt_text_img" />
		<Form onSubmit={e => loginUser(e)}>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="text" value={email} onChange={e => {setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}} required/>
			</Form.Group>
			{
				isActive
				?<Button type="submit" className="btnMar">Login</Button>
				:<Button type="submit" disabled className="btnMar">Login</Button>
			}
		</Form>
		</div>
		</Container>
		
	)
} 
