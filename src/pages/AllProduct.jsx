import React,{useState,useEffect} from 'react';
import AdminProduct from '../components/AdminProduct'
import {useParams,useHistory, Link} from 'react-router-dom';

export default function AllProduct(){
	const history = useHistory();
	const [prodDetails,setProductDetails] = useState([])
	let token = localStorage.getItem('token')
	useEffect(()=>{
		console.log(token)
		fetch(`https://sheltered-peak-35807.herokuapp.com/products/getAllProduct`,{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProductDetails(data.map(product => {
				return (
					<AdminProduct prodProp={product}/>
				)
			}))	
		})
		.catch(err =>{
			console.log(err)
			alert('error')
		})
	},[])

	return (
		<>
			<h1 className="my-5 text-center">All Products</h1>
			{prodDetails}
		</>
		)
}
