import {Form, Button, ButtonGroup, Container} from 'react-bootstrap';
import {RadioGroup, Radio} from 'react-radio-group';
import {Redirect,useHistory} from 'react-router-dom';
import React,{useState,useEffect,useContext} from 'react';
import UserContext from '../userContext';
import Swal from 'sweetalert2';
import '../srcCSS/universal.css';

export default function AddProduct(){

	const{user} = useContext(UserContext);

	const history = useHistory();

	const[name,setName] = useState("");
	const[desc,setdDesc] = useState("");
	const[price,setPrice] = useState(0);
	const[image,setImage] = useState(0);
	const[isActive,setIsActive] = useState(true);

	useEffect(()=>{
		if(name !== "" && desc !== "" && price !== 0){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	},[name, desc, price]);

	function AddProduct(e){

		e.preventDefault()

		let token = localStorage.getItem('token')

		fetch('https://sheltered-peak-35807.herokuapp.com/products/createProduct',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			name: name,
			desc: desc,
			price: price,
			image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Product Creation Failed.",
					text: data.message
				})	
				} 
			else {
				console.log(data)
				Swal.fire({
					icon: "success",
					title: "Product Creation Successful.",
					text: `Product has been created.`
				})
				history.push("/viewAdmin");
			}

		})
		setName("");
		setdDesc("");
		setPrice(0);
		setImage("");
	};

	return(
		<>
		<Container className="centerAdd">
			<h1 className = "text-center">Add Product</h1>
			<Form onSubmit={ e => AddProduct(e)}>
				<Form.Group controlId="name">
					<Form.Label>Product Name:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Name"
						value={name}
						onChange={(e) => setName(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="desc">
					<Form.Label>Description:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Product Description"
						value={desc}
						onChange={(e) => setdDesc(e.target.value)}
						required
					/>
				</Form.Group>
				<Form.Group controlId="price">
					<Form.Label>Price:</Form.Label>
					<Form.Control 
						type="number"
						placeholder="Enter Price"
						value={price}
						onChange={(e) => setPrice(e.target.value)}
						required
					/>
				</Form.Group>					
{/*				<Form.Group controlId="category">
					<Form.Label>Category:</Form.Label>
					<Form.Select aria-label="Default select example" onChange={(e) => setCat(e.target.value)}>
						<option>Select Category</option>
						<option value={isFigma}>Figma</option>
						<option value={isNendo}>Nendoroid</option>
						<option value={isFunko}>Funko Pop</option>
					</Form.Select>
				</Form.Group>*/}			
				<Form.Group controlId="image">
					<Form.Label>Image URL:</Form.Label>
					<Form.Control 
						type="text"
						placeholder="Enter Image URL"
						value={image}
						onChange={(e) => setImage(e.target.value)}
						required
					/>
				</Form.Group>
				{
					isActive 
					? <Button type="submit" variant="primary" className = "btnMar">Submit</Button>
					: <Button type="submit" variant="danger" disabled className = "btnMar">Submit</Button>
				}
			</Form>
			</Container>
		</>
	)
}