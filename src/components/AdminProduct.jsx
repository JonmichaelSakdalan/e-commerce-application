import React,{useState,useEffect} from 'react';
import {CardGroup, Card, Button, Row, Col, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../srcCSS/universal.css';


export default function Product({prodProp}){
	return (
	<Container>
		<Card className="p-1 m-2 minCard text-center">
			<Card.Img variant="top" style={{ height: '20rem'}} src={prodProp.image} />
		    <Card.Body>
		    	<Card.Title style={{ height: '3rem'}}>{prodProp.name}</Card.Title>
		    	<Card.Text style={{ height: '3rem'}}>{prodProp.desc}</Card.Text>
		   	 	<Card.Text>PHP: {prodProp.price }</Card.Text>
		   		<Link className="btn btn-secondary" to={`/viewAdmin/${prodProp._id}`}>View Status</Link>
			</Card.Body>
		</Card>
	</Container>
	)
}
