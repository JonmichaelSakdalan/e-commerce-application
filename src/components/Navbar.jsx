import React,{useContext} from 'react';
import {Navbar,Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../userContext'
import {BsFillCartFill} from 'react-icons/bs';
import '../srcCSS/universal.css';

export default function NavBar(){

	const {user} = useContext(UserContext);
	return(
		<Navbar bg="dark" variant="dark"  expand="lg" className="pt-3 pl-3 pr-3 ">
			<div  className="px-4">
			<Navbar.Brand as={Link} to="/">AnimeHub</Navbar.Brand>
			</div>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse>
				<Nav className="text-center fontsz ml-auto">
		    		<Nav.Link as={Link} to="/">Home</Nav.Link>
		    		<Nav.Link as={Link} to="/viewAll">Products</Nav.Link>
		    		{
		    			user.id
		    			?
		    				user.isAdmin
		    				?
		    				<>
		    					<Nav.Link as={Link} to="/addProduct">Add Products</Nav.Link>
		    					<Nav.Link as={Link} to="/viewAdmin">View All Products</Nav.Link>
		    					<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		    				</>
		    				:
		    				<>
		    				<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		    				</>
		    			:
		    			<>
		    				<Nav.Link as={Link} to="/login">Login</Nav.Link>
		    				<Nav.Link as={Link} to="/register">Register</Nav.Link>
		    			</>
		    		}
		    	</Nav>		 	
		    <Nav id="navLeft" >
		    	<Nav.Link className="mr-4"><BsFillCartFill style={{width:'4rem'}}/></Nav.Link>
		    </Nav>
		 	</Navbar.Collapse>

		</Navbar>
	)
}